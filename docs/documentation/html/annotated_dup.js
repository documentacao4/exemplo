var annotated_dup =
[
    [ "applications", null, [
      [ "info", null, [
        [ "InfoController", null, [
          [ "InfoController", "d5/d31/classapplications_1_1info_1_1_info_controller_1_1_info_controller.html", null ]
        ] ],
        [ "InfoModels", null, [
          [ "InfoModel", "d4/d94/classapplications_1_1info_1_1_info_models_1_1_info_model.html", null ]
        ] ]
      ] ],
      [ "param", null, [
        [ "ParamController", null, [
          [ "ParamController", "d8/db5/classapplications_1_1param_1_1_param_controller_1_1_param_controller.html", null ]
        ] ],
        [ "ParamModels", null, [
          [ "ParamModel", "da/d41/classapplications_1_1param_1_1_param_models_1_1_param_model.html", null ],
          [ "propriedade", "de/da6/classapplications_1_1param_1_1_param_models_1_1propriedade.html", null ]
        ] ]
      ] ],
      [ "simul", null, [
        [ "SimulController", null, [
          [ "SimulController", "d7/de3/classapplications_1_1simul_1_1_simul_controller_1_1_simul_controller.html", null ]
        ] ],
        [ "SimulModels", null, [
          [ "SimulModel", "da/db9/classapplications_1_1simul_1_1_simul_models_1_1_simul_model.html", null ]
        ] ]
      ] ],
      [ "test", null, [
        [ "TestController", null, [
          [ "TestController", "d5/d4b/classapplications_1_1test_1_1_test_controller_1_1_test_controller.html", null ]
        ] ],
        [ "TestModels", null, [
          [ "testModel", "d8/da5/classapplications_1_1test_1_1_test_models_1_1test_model.html", null ]
        ] ]
      ] ]
    ] ],
    [ "communicationHandler", null, [
      [ "communicationHandler", "d0/d8e/classcommunication_handler_1_1communication_handler.html", null ]
    ] ],
    [ "dataModel", null, [
      [ "SerialModel", "d1/dac/classdata_model_1_1_serial_model.html", null ]
    ] ],
    [ "mainController", null, [
      [ "MainController", "d8/dd7/classmain_controller_1_1_main_controller.html", null ]
    ] ],
    [ "serialHandler", null, [
      [ "SerialHandler", "d1/d1c/classserial_handler_1_1_serial_handler.html", null ]
    ] ]
];