var classserial_handler_1_1_serial_handler =
[
    [ "__init__", "d1/d1c/classserial_handler_1_1_serial_handler.html#a6d7278fc8c10065e61ce5416c172b6f1", null ],
    [ "addReceiver", "d1/d1c/classserial_handler_1_1_serial_handler.html#a19295cd2cf310a9110994ae1ff228329", null ],
    [ "addTransmitter", "d1/d1c/classserial_handler_1_1_serial_handler.html#ae200cbdc9026eb88e790341d3be1d40e", null ],
    [ "connectToPort", "d1/d1c/classserial_handler_1_1_serial_handler.html#a16f53a72a06ddf5358fe9bf9763f614d", null ],
    [ "getAvailablePorts", "d1/d1c/classserial_handler_1_1_serial_handler.html#aaea0848dd73977f52d0262b8f859d17a", null ],
    [ "getBaudrate", "d1/d1c/classserial_handler_1_1_serial_handler.html#a66dcc930a52313689e7d195e8e00336d", null ],
    [ "getCOMPort", "d1/d1c/classserial_handler_1_1_serial_handler.html#ad25d0ec667cb869016706a96566b4080", null ],
    [ "receiveData", "d1/d1c/classserial_handler_1_1_serial_handler.html#a3872a63842bc0d025f3aa8a8572b7d27", null ],
    [ "removeReceiver", "d1/d1c/classserial_handler_1_1_serial_handler.html#a59406df26190b5b01400b4afd9d38df4", null ],
    [ "testDataReception", "d1/d1c/classserial_handler_1_1_serial_handler.html#a5b265116f3d80f177c60ce299cfd6f52", null ],
    [ "testDataTransmission", "d1/d1c/classserial_handler_1_1_serial_handler.html#ab403c858dc919708a683853681deafba", null ],
    [ "transmitData", "d1/d1c/classserial_handler_1_1_serial_handler.html#ab32e9b8088c42c89363f5d0d128bb6a6", null ],
    [ "_portList", "d1/d1c/classserial_handler_1_1_serial_handler.html#a34a0d610e131af37359bcbebae0ab5fe", null ],
    [ "parent", "d1/d1c/classserial_handler_1_1_serial_handler.html#ad99f13c1e8b9efc617fc4eafc13701c6", null ],
    [ "testDataReception", "d1/d1c/classserial_handler_1_1_serial_handler.html#a6c757b855a42320809793c02a285cbbe", null ],
    [ "testDataTransmission", "d1/d1c/classserial_handler_1_1_serial_handler.html#aedd17182faf79145ec8c2290dff8270b", null ]
];